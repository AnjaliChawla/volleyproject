package com.primelite.docket;

import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Anjali Chawla on 13/6/17.
 */

public class Request implements ResponseHandler {
    private String requestUrl;
    private String requestType;
    private int requestMethod;
    private ArrayList<Param> paramList = new ArrayList<>();
    private VollyCallback mVollyCallback;

    public Request(VollyCallback vollyCallback) {
        if (vollyCallback != null) {
            this.mVollyCallback = vollyCallback;
        }
    }

    public Request setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
        return this;
    }

    public Request setRequestType(String requestType) {
        this.requestType = requestType;
        return this;
    }


    public Request setRequestMethod(int requestMethod) {
        this.requestMethod = requestMethod;
        return this;
    }


    public String getRequestUrl() {
        return requestUrl;
    }

    public String getRequestType() {
        return requestType;
    }

    public int getRequestMethod() {
        return requestMethod;
    }

    public Request setPerams(String key, Object values) {
        Param param = new Param();
        param.setKey(key);
        param.setValue(values);
        paramList.add(param);
        return this;
    }

    public ArrayList<Param> getParams() {
        return paramList;
    }

    @Override
    public void onGetResponse(Object obj) {
        System.out.println("anjali" + obj);
        try {
            if (obj.getClass().equals(String.class)) {
                System.out.println(" Response :  String Type");
                JSONObject jsonObject= new JSONObject((String) obj);
                mVollyCallback.onSuccess(jsonObject);
            }
            if (obj.getClass().equals(JSONObject.class)) {
                System.out.println(" Response :  JSONObject Type");
                JSONObject jsonObject= new JSONObject((String) obj);
                mVollyCallback.onSuccess(jsonObject);
            }
            if (obj.getClass().equals(JSONArray.class)) {
                System.out.println(" Response :  JSONArray Type");
            }
            if (obj.getClass().equals(Bitmap.class)) {
                System.out.println(" Response :  Image Type");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Param {
    private String key;
    private Object value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}


