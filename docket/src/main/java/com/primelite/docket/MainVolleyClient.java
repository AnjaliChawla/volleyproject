package com.primelite.docket;

import android.graphics.Bitmap;
import android.util.ArrayMap;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anjali Chawla on 9/6/17.
 */

public class MainVolleyClient {
    private static final String TAG = MainVolleyClient.class.getSimpleName();
    public static final int GET = 0;
    public static final int POST = 1;
    public static final int PUT = 2;
    public static final int DELETE = 3;
    public static final int PATCH = 4;

    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_TEXT_JSON = "text/json";
    public static final String HEADER_URL_ENCODED = "application/x-www-form-urlencoded";

    public static final String STRING_REQUEST = "stringRequest";
    public static final String JSON_ARRAY_REQUEST = "jsonArrayReques";
    public static final String JSON_OBJECT_REQUEST = "jsonObjectRequest";
    public static final String IMAGE_REQUEST = "imageRequest";
    private RequestQueue queue;

    public MainVolleyClient() {
        queue = Volley.newRequestQueue(BaseRequestAdapter.context);
    }


    public MainVolleyClient getMainVolleyClientObject() {
        return this;
    }


    public void execute(Request request) {
        if (request != null && Connectivity.isConnected()) {
            this.executeRequest(request);
        }
    }

    private void executeRequest(final Request request) {
        switch (request.getRequestType()) {
            case (STRING_REQUEST):

                StringRequest stringRequest = new StringRequest(request.getRequestMethod(), request.getRequestUrl(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        request.onGetResponse(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put(HEADER_CONTENT_TYPE, HEADER_URL_ENCODED);
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> paramsMap = new ArrayMap<>();
                        for (Param param : request.getParams()) {
                            paramsMap.put(param.getKey(), (String) param.getValue());
                        }
                        return paramsMap;
                    }
                };
                queue.add(stringRequest);
                break;
            case (JSON_ARRAY_REQUEST):
                break;
            case (JSON_OBJECT_REQUEST):
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(request.getRequestMethod(), request.getRequestUrl(), null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/x-www-form-urlencoded");
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> paramsMap = new ArrayMap<>();
                        for (Param param : request.getParams()) {
                            paramsMap.put(param.getKey(), (String) param.getValue());
                        }
                        return paramsMap;
                    }
                };

                queue.add(jsObjRequest);
                break;
            case (IMAGE_REQUEST):
                ImageRequest imageRequest = new ImageRequest(request.getRequestUrl(), new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {

                    }
                }, 100, 100, null, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
                queue.add(imageRequest);
                break;
            default:
                break;
        }
    }


}
