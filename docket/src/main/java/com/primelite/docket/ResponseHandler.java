package com.primelite.docket;

/**
 * Created by Anjali Chawla on 13/6/17.
 */

public interface ResponseHandler {
    void onGetResponse(Object obj);
}
