package com.primelite.volleydemo;

import com.primelite.docket.MainVolleyClient;
import com.primelite.docket.Request;
import com.primelite.docket.VollyCallback;

/**
 * Created by Anjali Chawla on 13/6/17.
 */

class TestRequest extends MainVolleyClient {


    public final static String REQUEST_URL = "http://85.13.213.94/agent_api/api_dashboard.php";
    public static final String KEY = "eb@2016_agent*62#a%";
    private String requestType;
    private String mobileNumber;


    public void execute(VollyCallback vollyCallback) {
        Request testRequest = new Request(vollyCallback)
                .setRequestUrl(REQUEST_URL)
                .setRequestMethod(POST)
                .setRequestType(getRequestType())
                .setPerams("key", KEY)
                .setPerams("mobile_number", getMobileNumber());
        super.execute(testRequest);
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }
}
